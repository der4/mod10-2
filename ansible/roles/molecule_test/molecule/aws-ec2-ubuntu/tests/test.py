import testinfra

def test_os_release(host):
    assert host.file("/etc/os-release").contains("Ubuntu")


def test_nginx_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    #assert nginx.version.startswith("1.16")
